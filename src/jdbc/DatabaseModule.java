package jdbc;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import model.ModelCustomers;

public class DatabaseModule {

	private static Connection connect = null;
    private static Statement statement = null;
    private static PreparedStatement preparedStatement = null;
    private static ResultSet resultSet = null;
    
    public static void connectToDatabase() throws Exception {
    	
    	try {
            Class.forName("com.mysql.jdbc.Driver");
            connect = DriverManager.getConnection("jdbc:mysql://localhost/feedback?user=sqluser&password=sqluserpw");
    	} catch (Exception e) {
            throw e;
        }
    	
    }
    
    public static void saveUser(ModelCustomers user) throws Exception {
        
    	try {
            
        	connectToDatabase();

            preparedStatement = connect.prepareStatement("insert into feedback.comments values (default, ?, ?, ?, ? , ?, ?)");

            preparedStatement.setString(1, user.getName());
            preparedStatement.setString(2, user.getSurname());
            // TUTAJ MUSISZ DOPISA�
            preparedStatement.executeUpdate();


        } catch (Exception e) {
            throw e;
        } finally {
            close();
        }

    }
    
    
    private void close() {
    	
        try {
            if (resultSet != null) {
                resultSet.close();
            }

            if (statement != null) {
                statement.close();
            }

            if (connect != null) {
                connect.close();
            }
        } catch (Exception e) {

        }
    }
    
	
}
