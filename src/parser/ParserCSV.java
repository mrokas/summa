package parser;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

import jdbc.DatabaseModule;
import model.ModelCustomers;


public class ParserCSV implements Parser {
    
	
	private ModelCustomers populateCustomers(String[] data){
        
		ModelCustomers customer = new ModelCustomers();
        customer.setName(data[0]);
        customer.setSurname(data[1]);
        if(data[2].isEmpty()){
        customer.setAge(0);
        }else{
        customer.setAge(Integer.parseInt(data[2]));
        }
        customer.setCity(data[3]);
        
        System.out.println(customer.getName()+"  "+ customer.getSurname()+""+customer.getAge()+""+customer.getCity()+"   ");
    	return customer;
    }
	

	@Override
	public void parse() {
		File file = new File("dane-osoby.txt");
    	String[] tempDataTable;
    	ModelCustomers cust;
    	
    	try (BufferedReader br = new BufferedReader(new FileReader(file))) {
	        
    		String line;
	        while ((line = br.readLine()) != null) {
	           
	            tempDataTable = line.split(",");
	            cust = populateCustomers(tempDataTable);
	            // TUTAJ METODA DODAJACA DO BAZY DANYCH
	            DatabaseModule.saveUser(cust);
	           
	        }
        
	    }catch(Exception e){
	 
	    }
	}


   
   
}